﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : Singleton<GameSceneManager>
{
    [SerializeField] protected List<TileDetails> m_AllTiles;

    protected List<int> bluePlayerTileIDList = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52, 53, 54, 55, 56, 57};
    protected List<int> yellowPlayerTileIDList = new List<int>() { 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 58, 59, 60, 61, 62, 63};
    protected List<int> redPlayerTileIDList = new List<int>() { 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 64, 65, 66, 67, 68, 69};
    protected List<int> greenPlayerTileIDList = new List<int>() { 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 70, 71, 72, 73, 74, 75};

    public int DiceResult { get; protected set; } = 6;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            DiceResult = Utility.GetRandomInt(1, 7);
    }

    public int GetResultTileID(int currentTileID, int noOfPositions, PlayerColor playerColor)
    {
        int resultTileID = -1;

        switch (playerColor)
        {
            case PlayerColor.Blue:
                if (currentTileID != -1)
                {
                    int currentTileIndex = bluePlayerTileIDList.FindIndex(x => x.Equals(currentTileID));
                    resultTileID = bluePlayerTileIDList[currentTileIndex + noOfPositions];
                }
                else
                {
                    resultTileID = bluePlayerTileIDList[0];
                }

                break;

            case PlayerColor.Yellow:
                if (currentTileID != -1)
                {
                    int currentTileIndex = yellowPlayerTileIDList.FindIndex(x => x.Equals(currentTileID));
                    resultTileID = yellowPlayerTileIDList[currentTileIndex + noOfPositions];
                }
                else
                {
                    resultTileID = yellowPlayerTileIDList[0];
                }

                break;

            case PlayerColor.Red:
                if (currentTileID != -1)
                {
                    int currentTileIndex = redPlayerTileIDList.FindIndex(x => x.Equals(currentTileID));
                    resultTileID = redPlayerTileIDList[currentTileIndex + noOfPositions];
                }
                else
                {
                    resultTileID = redPlayerTileIDList[0];
                }

                break;

            case PlayerColor.Green:
                if (currentTileID != -1)
                {
                    int currentTileIndex = greenPlayerTileIDList.FindIndex(x => x.Equals(currentTileID));
                    resultTileID = greenPlayerTileIDList[currentTileIndex + noOfPositions];
                }
                else
                {
                    resultTileID = greenPlayerTileIDList[0];
                }

                break;
        }

        return resultTileID;
    }

    public Vector3 GetWorldPositionOfTile(int tileID)
    {
        TileDetails tile = m_AllTiles.Find(x => x.TileID == tileID);

        if (tile != null)
            return tile.transform.position;

        return Vector3.zero;
    }

    public List<Vector3> GetTilePointsInBetween(int startingTileID, int endingTileID, PlayerColor playerColor)
    {
        List<Vector3> inBetweenPoints = new List<Vector3>();

        switch (playerColor)
        {
            case PlayerColor.Blue:
                if (startingTileID != -1)
                {
                    int currentTileIndex = bluePlayerTileIDList.FindIndex(x => x.Equals(startingTileID));
                    int endTileIndex = bluePlayerTileIDList.FindIndex(x => x.Equals(endingTileID));

                    for (int i = currentTileIndex; i <= endTileIndex; i++)
                    {
                        inBetweenPoints.Add(GetWorldPositionOfTile(bluePlayerTileIDList[i]));
                    }
                }

                break;

            case PlayerColor.Yellow:
                if (startingTileID != -1)
                {
                    int currentTileIndex = yellowPlayerTileIDList.FindIndex(x => x.Equals(startingTileID));
                    int endTileIndex = yellowPlayerTileIDList.FindIndex(x => x.Equals(endingTileID));

                    for (int i = currentTileIndex; i <= endTileIndex; i++)
                    {
                        inBetweenPoints.Add(GetWorldPositionOfTile(yellowPlayerTileIDList[i]));
                    }
                }

                break;

            case PlayerColor.Red:
                if (startingTileID != -1)
                {
                    int currentTileIndex = redPlayerTileIDList.FindIndex(x => x.Equals(startingTileID));
                    int endTileIndex = redPlayerTileIDList.FindIndex(x => x.Equals(endingTileID));

                    for (int i = currentTileIndex; i <= endTileIndex; i++)
                    {
                        inBetweenPoints.Add(GetWorldPositionOfTile(redPlayerTileIDList[i]));
                    }
                }

                break;

            case PlayerColor.Green:
                if (startingTileID != -1)
                {
                    int currentTileIndex = greenPlayerTileIDList.FindIndex(x => x.Equals(startingTileID));
                    int endTileIndex = greenPlayerTileIDList.FindIndex(x => x.Equals(endingTileID));

                    for (int i = currentTileIndex; i <= endTileIndex; i++)
                    {
                        inBetweenPoints.Add(GetWorldPositionOfTile(greenPlayerTileIDList[i]));
                    }
                }

                break;
        }

        return inBetweenPoints;
    }
}
