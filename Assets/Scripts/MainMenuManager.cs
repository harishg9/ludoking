﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : Singleton<MainMenuManager>
{
    [SerializeField] protected Button m_TwoPlayerButton;

    [SerializeField] protected Button m_ThreePlayerButton;

    [SerializeField] protected Button m_FourPlayerButton;

    [SerializeField] protected Button m_QuitGameButton;

    protected int noOfPlayers;

    // Start is called before the first frame update
    void Start()
    {
        m_TwoPlayerButton.onClick.AddListener(() => { TwoPlayersButtonClicked(); });
        m_ThreePlayerButton.onClick.AddListener(() => { ThreePlayersButtonClicked(); });
        m_FourPlayerButton.onClick.AddListener(() => { FourPlayersButtonClicked(); });
        m_QuitGameButton.onClick.AddListener(() => { QuitButtonClicked(); });

        SceneManager.sceneLoaded += OnSceneLoaded;  
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Button UI Listeners

    public void TwoPlayersButtonClicked()
    {
        noOfPlayers = 2;
        StartCoroutine(LoadGameSceneAsync());
    }

    public void ThreePlayersButtonClicked()
    {
        noOfPlayers = 3;
        StartCoroutine(LoadGameSceneAsync());
    }

    public void FourPlayersButtonClicked()
    {
        noOfPlayers = 4;
        StartCoroutine(LoadGameSceneAsync());
    }

    public void QuitButtonClicked()
    {
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    #endregion Button UI Listeners

    IEnumerator LoadGameSceneAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);

        while (!asyncLoad.isDone)
            yield return new WaitForEndOfFrame();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        PlayerManager.Instance.EnablePlayers(noOfPlayers);
    }
}