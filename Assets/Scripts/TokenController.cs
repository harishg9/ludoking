﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenController : MonoBehaviour
{
    protected int currentTileID = -1;

    protected PlayerColor tokenPlayerColor;

    protected Vector3 homePosition;

    protected bool isAtHome = true;

    // Start is called before the first frame update
    void Start()
    {
        homePosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTokenPlayerColor (PlayerColor playerColor)
    {
        tokenPlayerColor = playerColor;
        this.GetComponent<SpriteRenderer>().sortingOrder = 4;
    }

    void MoveTokenTo(int tileID)
    {
        Vector3 positionToMoveTo = GameSceneManager.Instance.GetWorldPositionOfTile(tileID);
        List<Vector3> inBetweenPoints = GameSceneManager.Instance.GetTilePointsInBetween(currentTileID, tileID, tokenPlayerColor);
        if (inBetweenPoints.Count > 0)
            iTween.MoveTo(this.gameObject, iTween.Hash("position", positionToMoveTo, "path", inBetweenPoints.ToArray(), "islocal", false, "speed", 10f, "easetype", iTween.EaseType.linear));
        else
            iTween.MoveTo(this.gameObject, iTween.Hash("position", positionToMoveTo, "islocal", false, "speed", 10f, "easetype", iTween.EaseType.linear));
        currentTileID = tileID;
    }

    void OnMouseDown()
    {
        if (isAtHome)
        {
            if (GameSceneManager.Instance.DiceResult == 6)
            {
                isAtHome = false;
                int tileIDToMoveTo = GameSceneManager.Instance.GetResultTileID(currentTileID, GameSceneManager.Instance.DiceResult, tokenPlayerColor);
                MoveTokenTo(tileIDToMoveTo);
            }
        }
        else
        {
            int tileIDToMoveTo = GameSceneManager.Instance.GetResultTileID(currentTileID, GameSceneManager.Instance.DiceResult, tokenPlayerColor);
            MoveTokenTo(tileIDToMoveTo);
        }
    }
}
