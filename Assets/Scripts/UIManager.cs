﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] protected Button m_BackButton;

    // Start is called before the first frame update
    void Start()
    {
        m_BackButton.onClick.AddListener(() => { BackButtonPressed(); });
    }

    // Update is called once per frame
    void Update()
    {

    }

    void BackButtonPressed()
    {
        StartCoroutine(LoadManinSceneAsync());
    }

    IEnumerator LoadManinSceneAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);

        while (!asyncLoad.isDone)
            yield return new WaitForEndOfFrame();
    }
}
