﻿using System.Collections;
using System.Collections.Generic;
using System;

public static class Utility
{
    public static int GetRandomInt (int minRange, int maxRange)
    {
        Random rand = new Random(System.DateTime.Now.Millisecond);
        return rand.Next(minRange, maxRange);
    }
}