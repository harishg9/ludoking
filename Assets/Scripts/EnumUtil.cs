﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerColor
{
    None,
    Blue,
    Yellow,
    Red,
    Green
}