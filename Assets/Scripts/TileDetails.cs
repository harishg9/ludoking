﻿using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDetails : MonoBehaviour
{
    [SerializeField] protected int m_TileID;
    public int TileID { get { return m_TileID; } }

    [SerializeField] protected bool m_IsStartingTile;

    [ConditionalField("m_IsStartingTile")] [SerializeField] protected PlayerColor m_StartingPointOfPlayer;

    [SerializeField] protected bool m_IsSafeTile;

    [SerializeField] protected bool m_IsCheckPointTile;

    [ConditionalField("m_IsCheckPointTile")] [SerializeField] protected PlayerColor m_CheckPointOfPlayer;

    [SerializeField] protected bool m_IsTilePLayerSpecific;

    [ConditionalField("m_IsTilePLayerSpecific")] [SerializeField] protected PlayerColor m_TileTypeOfPlayer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
