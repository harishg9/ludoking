﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager>
{
    [SerializeField] protected List<PlayerController> m_AllPlayers;

    protected List<PlayerController> activePlayersList = new List<PlayerController>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnablePlayers(int playerCount)
    {
        for (int i = 0; i < playerCount; i++)
        {
            m_AllPlayers[i].JoinPlayer();
            activePlayersList.Add(m_AllPlayers[i]);
        }
    }
}