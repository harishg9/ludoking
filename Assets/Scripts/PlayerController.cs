﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] protected GameObject m_GreyedOverlayObj;

    [SerializeField] protected List<TokenController> m_PlayerTokenList;

    [SerializeField] protected PlayerColor m_PlayerColor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void JoinPlayer()
    {
        m_GreyedOverlayObj.SetActive(false);

        foreach (TokenController token in m_PlayerTokenList)
        {
            token.SetTokenPlayerColor(m_PlayerColor);
        }
    }
}
